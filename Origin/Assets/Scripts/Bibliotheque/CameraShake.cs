﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public Animator camAnim;
  
    public void CamShake()
    {
        camAnim.SetBool("shake", true);
        //Debug.Log("Shake");
     
    }
    public void Idle()
    {
        camAnim.SetBool("shake", false);
        //Debug.Log("Idle");

    }
}
