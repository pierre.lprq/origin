﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script Camera Turn around a target and zoom

public class CameraOrbit : MonoBehaviour
{


    public GameObject target;
    //public GameObject CameraFollowMouse;
    float speed = 5;
    Rigidbody m_Rigidbody;

    public int zoom = 20;
    public int normal = 60;
    public float smooth = 5; //for transition between normal and zoom

    private bool isZoomed = false;
    private float startY;



    void Start()
    {

        m_Rigidbody = GetComponent<Rigidbody>();
        startY = transform.position.y;
    }


    // Update is called once per frame
    void Update()
    {
        // clic gauche : deplacement autour de l'objet
        if (Input.GetMouseButton(0))
        {
            transform.RotateAround(target.transform.position, Vector3.up, Input.GetAxis("Mouse X") * speed);
        }

        //Zoom
        if (Input.GetMouseButtonDown(1)) // clic droit : zoom
        {
            isZoomed = !isZoomed;
        }

        if(isZoomed)
        {
            GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetComponent<Camera>().fieldOfView, zoom, Time.deltaTime * smooth);

            float currY = transform.position.y;
            currY += Input.GetAxis("Vertical") * Time.deltaTime * 1000f;
            currY = Mathf.Clamp(currY, 10, 50);
            Vector3 targ = transform.position;
            targ.y = currY;
            transform.position = Vector3.Lerp(transform.position, targ, Time.deltaTime * smooth);

        }

        else
        {
            GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetComponent<Camera>().fieldOfView, normal, Time.deltaTime * smooth);

            Vector3 targ = transform.position;
            targ.y = startY;
            transform.position = Vector3.Lerp(transform.position, targ, Time.deltaTime * smooth);
        }
        
    }
}
